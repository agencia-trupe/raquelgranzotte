@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto inicial') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}