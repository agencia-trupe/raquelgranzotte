@extends('frontend.common.template')

@section('content')

<div class="home">
    <div class="dados-empresa">
        <div class="institucional">
            {!! $institucional->texto !!}
        </div>
        <div class="itens-servicos">
            @foreach($servicos as $item)
            <div class="item">
                <img src="{{ asset('assets/img/servicos/'.$item->imagem) }}" alt="">
                <div class="item-texto">
                    {!! $item->texto !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="contatos">
        <div class="contato">
            <p class="nome">{{ $contato->nome }}</p>
            <p class="registro">{{ $contato->registro }}</p>
            <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="telefone" target="_blank">{{ $contato->telefone }}</a>
        </div>
        <div class="form-contato">
            <h2>Fale Conosco</h2>
            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>

                <button type="submit" class="btn-contato">Enviar</button>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif

                @if(session('enviado'))
                <div class="flash flash-sucesso">
                    Mensagem enviada com sucesso!
                </div>
                @endif
            </form>
        </div>
    </div>
</div>

@endsection