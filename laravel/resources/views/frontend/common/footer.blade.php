<footer>
    <div class="footer">
        <a href="{{ route('home') }}" class="logo-rodape">
            <img src="{{ asset('assets/img/layout/icone-granzotte.svg') }}" alt="">
            <div class="info-cliente">
                <p>{{ $contato->nome }}</p>
                <p>Advogada</p>
            </div>
        </a>
        <div class="linha-vertical"></div>
        <div class="info-direitos">
            <p>© {{ date('Y') }} {{ $contato->nome }}</p>
            <p>Todos os direitos reservados</p>
        </div>
    </div>
</footer>