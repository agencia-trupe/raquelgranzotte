<header>
    <div class="center">
        <div class="bg-logo"></div>
        <a href="{{ route('home') }}" class="logo" alt="{{ $config->title }}"></a>
        <!-- <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button> -->
        <nav>
            @include('frontend.common.nav')
        </nav>
    </div>
</header>