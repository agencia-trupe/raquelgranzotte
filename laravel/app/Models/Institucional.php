<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];
}
