<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Institucional;
use App\Models\Servico;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $institucional = Institucional::first();

        $servicos = Servico::ordenados()->get();

        $contato = Contato::first();

        $whatsapp = "55".str_replace(" ", "", $contato->telefone);
        
        return view('frontend.home', compact('institucional', 'servicos', 'contato', 'whatsapp'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return redirect('/')->with('enviado', true);
    }

    private function sendMail($data) // CONTATO
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('emails.contato', $data, function ($m) use ($email, $data) {
            $m->to($email, config('app.name'))
                ->subject('[CONTATO] ' . config('app.name'))
                ->replyTo($data['email'], $data['nome']);
        });
    }
}
