<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Institucional;
use App\Http\Requests\InstitucionalRequest;

class InstitucionalController extends Controller
{
    public function index()
    {
        $registro = Institucional::first();

        return view('painel.institucional.edit', compact('registro'));
    }

    public function update(InstitucionalRequest $request, Institucional $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.institucional.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
