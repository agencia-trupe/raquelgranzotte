<?php

namespace App\Http\Controllers\Painel;

use App\Models\Configuracoes;
use App\Http\Requests\ConfiguracoesRequest;
use App\Http\Controllers\Controller;

class ConfiguracoesController extends Controller
{
    public function index()
    {
        $registro = Configuracoes::first();

        return view('painel.configuracoes.edit', compact('registro'));
    }

    public function update(ConfiguracoesRequest $request, Configuracoes $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_de_compartilhamento'])) $input['imagem_de_compartilhamento'] = Configuracoes::upload_imagem_de_compartilhamento();

            $registro->update($input);

            return redirect()->route('painel.configuracoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
