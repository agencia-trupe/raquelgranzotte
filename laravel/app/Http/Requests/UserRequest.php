<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            $rules['email'] = 'required|email|max:255|unique:users,email,' . $this->route('usuarios')->id;
            $rules['password'] = 'confirmed|min:6';
        }

        return $rules;
    }
}
