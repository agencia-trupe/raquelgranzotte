<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'        => 'required',
            'email'       => 'required|email',
            'telefone'    => 'required',
            'registro'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'email'    => 'Preencha todos os campos corretamente',
        ];
    }
}
