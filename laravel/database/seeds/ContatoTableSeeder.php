<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome' => 'Raquel Moreira Granzotte',
            'email' => 'contato@trupe.net',
            'telefone' => '11 98982 8025',
            'registro' => 'OAB/SP 217259',
        ]);
    }
}
