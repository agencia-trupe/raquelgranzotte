<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'trupe',
            'email'    => 'contato@trupe.net',
            'password' => bcrypt('senhatrupe'),
        ]);
    }
}
